package org.sercabni.sorting;
import java.util.Arrays;

public class App {
    public static void main(String[] args) {
        int[] numbers = Arrays.stream(args).mapToInt(Integer::parseInt).toArray();
        Arrays.sort(numbers);
        System.out.println(Arrays.toString(numbers));
    }
}

